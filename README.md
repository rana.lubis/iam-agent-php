# IamPln Php Agent

Iam Pln Php Agent merupakan library untuk memudahkan integrasi antara aplikasi yang dibangun menggunakan bahasa pemrograman PHP, dengan atau tanpa framework bantuan seperti Codeigniter (CI) atau Laravel.

**NOTICE:** IamPln-Php-Agent hanya mendukung php 5 keatas. Latest version is 1.22.

**Table of Contents**

<!-- TOC depthFrom:2 depthTo:3 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Installation](#installation)
- [Configurations](#configurations)
- [Authentication](#authentication)
- [Authorization](#authorization)

<!-- /TOC -->

## Installation

Modify composer.json to include iam repositories and package
```
"repositories": [
	...,
	{
		"type": "vcs",
		"url":  "https://gitlab.com/rana.lubis/iam-agent-php.git"
	}
],
"require": {
	...,
	"ranalubis/iam-agent-php": "1.22"
}
```
Reload composer library
```
composer update
```
### Plain PHP
Include composer autoloader
```php
require __DIR__ . '/vendor/autoload.php';
```
### Codeigniter
Include composer autoloader pada file ./application/config/config.php
```php
$config['composer_autoload'] = "vendor/autoload.php";
```
### Laravel
No need to modify anything since laravel will autoload automatically

## Configuration
### Codeigniter
Add iam server properties in codeigniter ./application/config/config.php
```php
$config['iam_server'] = array(
	"issuer" => "https://iamlocal.pln.co.id/oauth2",
	"auth_endpoint" => "https://iamlocal.pln.co.id/oauth2/auth",
	"token_endpoint" => "https://iamlocal.pln.co.id/oauth2/token",
	"userinfo_endpoint" => "https://iamlocal.pln.co.id/oauth2/me",
	"client_id" => $clientId, // <-- change this
	"client_secret" => $clientSecret, // <-- change this
	"redirect_uri" => $redirectUri, // <-- change this
	"logout_uri" => "https://iamlocal.pln.co.id/oauth2/logout",
	"scopes" => array("openid", "email", "phone", "profile", "empinfo", "address"), // <--- change this
	"logging" => true
);
```
### Laravel
Add iam server properties in laravel ./config/app.php
```php
'iam_server' => array(
        "issuer" => "https://iamlocal.pln.co.id/oauth2",
        "auth_endpoint" => "https://iamlocal.pln.co.id/oauth2/auth",
        "token_endpoint" => "https://iamlocal.pln.co.id/oauth2/token",
        "userinfo_endpoint" => "https://iamlocal.pln.co.id/oauth2/me",
        "client_id" => $clientId, // <-- change this
        "client_secret" => $clientSecret, // <-- change this
        "redirect_uri" => $redirectUri, // <-- change this
        "logout_uri" => "https://iamlocal.pln.co.id/oauth2/logout",
        "scopes" => array("openid", "email", "phone", "profile", "empinfo", "address"),
        "logging" => true
    )
```
## Authentication
### Plain PHP
Tambahkan code seperti berikut pada tiap PHP file yang akan di protect dengan IAM
```php
<?php
	namespace IamPln;
    
    require __DIR__ . '/vendor/autoload.php';
    
    use IamPln\SsoAgent;
    
    $agent = new SsoAgent(
		'https://iamlocal.pln.co.id/oauth2', 
		'https://iamlocal.pln.co.id/oauth2/auth', 
		'https://iamlocal.pln.co.id/oauth2/token',
		'https://iamlocal.pln.co.id/oauth2/me',
		$clientId, // <-- change this
		$clientSecret, // <-- change this
		$redirectUri, // <-- change this
		'https://iamlocal.pln.co.id/oauth2/logout',
		array("openid", "email", "phone", "profile", "empinfo", "address"),
		true,
        true
	);
    
    if(!$agent->isAuthenticated())
    	$agent->authenticate();
        
    echo "Hello world!!!";
?>
```
### Codeigniter
Tambahkan code seperti berikut pada tiap Controller yang akan di protect dengan IAM
```php
<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  use IamPln\SsoSecureController;

  class Welcome extends SsoSecureController {

      public function __construct() {
          parent::__construct();
      }

      public function index()
      {
          $this->load->view('welcome_message');
      }
  }
?>
```
### Laravel
Modifikasi file ./app/Providers/AppServiceProvider.php dengan code seperti berikut
```php
<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Log;
use IamPln\SsoAppServiceProvider;
use Illuminate\Contracts\Auth\Authenticatable;
use App\User;

class AppServiceProvider extends SsoAppServiceProvider // <-- extend sso service provider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot(); // <-- call parent boot function
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        parent::register(); // <-- call parent register function
    }

    public function findUser(string $identifier): Authenticatable {
        return User.where('username', $identifier).first(); // <-- change this with your implementation
    }
}
```
Modifikasi file ./app/Providers/AuthServiceProvider.php dengan code seperti berikut
```php
<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use IamPln\SsoAuthServiceProvider;

class AuthServiceProvider extends SsoAuthServiceProvider // <-- extend sso auth service
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot(); // <-- call parent register function
    }
}
```
Modifikasi file model user ./app/User.php dengan code seperti berikut
```php
<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use IamPln\SsoUser;

class User extends Authenticatable
{
    use Notifiable;
    use SsoUser; // <-- use trait sso user

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
```
Modifikasi file ./config/auth.php dengan menambahkan sso guard seperti berikut
```php
'guards' => [
  ...,

  'sso' => [
  	'driver' => 'ssoagent'
  ]
]
```
Tambahkan code seperti berikut pada tiap controller yang akan di protect dengan IAM
```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:sso'); // <-- tambahkan authentikasi dengan sso
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
}
```
Atau modifikasi file router dengan code seperti berikut
```php
Route::get('/', function () {
    //
})->middleware('auth:sso');
// atau
Route::group(['middleware' => ['auth:sso']], function () {
    //
});
```
## Authorization
Untuk dapat mengiplementasikan autorisasi atau privilege restrictions, di sarankan melakukan setting role atau permissions pada dashboard IAM untuk aplikasi yang akan di autorisasikan.
### Plain PHP
Tambahkan role checking pada tiap file php yang akan dilakukan autorisasi seperti berikut.
```php
<?php
	namespace IamPln;
    
    require __DIR__ . '/vendor/autoload.php';
    
    use IamPln\SsoAgent;
    
    $agent = new SsoAgent(
		'https://iamlocal.pln.co.id/oauth2', 
		'https://iamlocal.pln.co.id/oauth2/auth', 
		'https://iamlocal.pln.co.id/oauth2/token',
		'https://iamlocal.pln.co.id/oauth2/me',
		$clientId, // <-- change this
		$clientSecret, // <-- change this
		$redirectUri, // <-- change this
		'https://iamlocal.pln.co.id/oauth2/logout',
		array("openid", "email", "phone", "profile", "empinfo", "address"),
		true,
        true
	);
    
    if(!$agent->isAuthenticated())
    	$agent->authenticate();
    
    if(!$agent->hasRole('admin'))
    	
    echo "Hello world!!!";
?>
```
### Codeigniter
Modifikasi file controller yang akan di autorisasi dengan menambahkan role seperti berikut.
```php
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use IamPln\SsoSecureController;

class Welcome extends SsoSecureController {

	public function __construct() {
		parent::__construct(array('admin')); // <-- tambahkan role apa saja yang di allow
	}

	public function index()
	{
		$this->load->view('welcome_message');
	}
}

```
### Laravel
Modifikasi file ./app/Http/Kernel.php dengan menambahkan middleware route seperti berikut
```php
protected $routeMiddleware = [
  ...,
  'ssorole' => \IamPln\SsoCheckRole::class
];
```
Modifikasi file controller yang akan di autorisasi dengan menambahkan middleware seperti berikut
```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sso');
        $this->middleware('ssorole:admin'); // <-- tambahkan middleware role guard
    }

    public function index()
    {
        return view('home');
    }
}
```
Atau dapat juga dilakukan modifikasi pada router seperti berikut
```php
Route::get('/', function () {
    //
})->middleware('auth:sso', 'ssorole:admin'); // <-- tambahkan middleware role guard
// atau
Route::group(['middleware' => ['auth:sso', 'ssorole:admin']], function () { // <-- tambahkan middleware role guard
    //
});
```