<?php
	namespace IamPln;

	require __DIR__ . '/vendor/autoload.php';
	require __DIR__ . '/src/SsoAgent.php';

	use IamPln\SsoAgent;
	
	if(isset($_GET['complete']))
		echo 'logged out';
	else {
		$agent = new SsoAgent(
			'https://iamlocal.pln.co.id/oauth2', 
			'https://iamlocal.pln.co.id/oauth2/auth', 
			'https://iamlocal.pln.co.id/oauth2/token',
			'https://iamlocal.pln.co.id/oauth2/me',
			'codeigniter',
			'codeigniter',
			'http://citest.pln.co.id:8090/iam-php-client/',
			'https://iamlocal.pln.co.id/oauth2/logout',
			array("openid", "email", "phone", "profile", "empinfo", "address"),
			true);
	
		$userMeta = $agent->getIdTokenPayload();
	
		if(isset($_GET['signout'])) 
			$agent->signOut('http://citest.pln.co.id:8090/iam-php-client/?complete=1');
		else 
			echo 'hello world ' . $userMeta['sub'];
	}

?>